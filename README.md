# AX88179和RTL8153-USB转网口芯片对比

#### 介绍
由于需要使用网卡进行ROS的topics的传输，基于推荐的AX88179和RTL8153（USB转网口芯片）进行对比。对比的数据来源于[AX88179官方文档](https://www.asix.com.tw/products.php?op=pItemdetail&PItemID=131;71;112)和[RTL8153官方文档](https://www.realtek.com/en/products/communications-network-ics/item/rtl8153)

#### 基本参数

|项目名|AX88179|RTL8153|
|:--:|:--:|:--:|
|传输速率|10/100/1000Mbps|10/100/1000Mbps|
|USB支持|USB 3.0\2.0\1.1|USB 3.0\2.0\1.1|
|支持协议|IPv4(IP/TCP/UDP/ICMP/IGMP)IPv6(TCP/UDP/ICMPv6)|IPv4/IPv6/TCP/UDP|
|电源管理（不同方式）|支持|支持|
|Crossover Detection & Auto-Correction|包含|包含|
|Wake-on-LAN|支持|支持|

#### IEEE
|项目名|AX88179|RTL8153|
|:--:|:--:|:--:|
|IEEE 802.3x|兼容|兼容|
|IEEE 802.3|兼容|兼容|
|IEEE 802.3u|兼容|兼容|
|IEEE 802.3ab|兼容||
|IEEE 802.1P|Layer 2 Priority Encoding and Decoding|Layer 2 Priority Encoding|
|IEEE 802.1Q|VLAN tagging and 2 VLAN ID filtering|VLAN tagging|
|IEEE 802.3az|兼容|IEEE 802.3az-2010|

#### 电源管理

##### AX88179
Advanced Power Management Features

* Supports power management offload (ARP & NS)
* Supports dynamic power management to reduce power dissipation during idle or light traffic
* Supports AutoDetach power saving
* Supports advanced link down power saving when Ethernet cable is unplugged
    
##### RTL8153
Intel CPPM (Converged Platform Power Management)
    
* Supports L1 with 3ms BESL (USB 2.0)
* Dynamic LTM messaging (USB 3.0)
* Supports U1/U2 (USB 3.0)
* Supports selective suspend

#### 总结

总体来说，两种USB转网口的芯片在大体的功能上没有很大的区别。相关功能从参数上来说较为一致。但部分功能如电源管理实现方式不同。可根据相关需求选择。

#### 参考
1. [AX88179官方文档](https://www.asix.com.tw/products.php?op=pItemdetail&PItemID=131;71;112)
2. [RTL8153官方文档](https://www.realtek.com/en/products/communications-network-ics/item/rtl8153)
